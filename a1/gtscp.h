#include <stdio.h>
#include <gcrypt.h>
#include <sys/stat.h> //stat
#include <errno.h> // fsize strerror

#define MAX_PASSWD_SZ 128  // maximum password length
#define PASSWD_BUFFER_SZ 16 // password buffer size
#define PBKDF2_SALT "NaCl"  // salt used for PBKDF2 algorightm
#define DEFAULT_IV 5844  // default iv for PBKDF2 algorightm
#define MAX_BUFFER_SZ 1024*1024  // 1 MB

/**
 * Handle socket error
 */
void handle_error(gcry_error_t err);

/**
 * Test if a file exists
 */
int file_exist(char *filename);

/**
 * Get file size
 */
off_t fsize(const char *filename);

/**
 * Write buffer to a file
 */
int gt_write(char *buffer, int buffer_size, char *filename);
