##Assignment2 for CS6262 Network Security

### Requirement

Library: libgcrypt

###Installation:

    $ make

###Usage:

    $ ./techrypt -h
    OVERVIEW: file encryption/transmission suite across network
    
    USAGE: techrypt < input file > [-d < IP-addr:port >] [-l ]
	OPTIONS:
	-d <IP-addr:port>  Transmit file to IP address/port
	-l                 Local mode, only encrypt file
	-h, -?             Display help message
	
    $ ./techdec -h
    OVERVIEW: file encryption/transmission suite across network

	USAGE: 	 techdec < filename > [-d < port >] [-l ]
	OPTIONS:
	-d <port>          listening port
	-l                 Local mode, only encrypt file
	-h, -?             Display help message
    

@Author zzhang356@mail.gatech.edu   
GTID: 902939260   
Last modified: 01/27/2014   
