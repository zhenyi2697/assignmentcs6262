#include <stdio.h>
#include <string.h> //strlen
#include <sys/socket.h>
#include <netdb.h> // hostent
#include <arpa/inet.h> //inet_addr: convert string to int ip
#include <unistd.h> // close socket
#include <gcrypt.h>
#include <sys/stat.h> //stat
#include <sys/types.h>
#include <errno.h> // fsize strerror

#include "gtscp.h"

/**
 * Display command usage
 */
void display_usage()
{
    printf("OVERVIEW: file encryption/transmission suite across network\n\n");
    printf("USAGE: \t techrypt < input file > [-d < IP-addr:port >] [-l ]\nOPTIONS:\n");
    printf("-d <IP-addr:port>  Transmit file to IP address/port\n");
    printf("-l                 Local mode, only encrypt file\n");
    printf("-h, -?             Display help message\n");
}

/**
 * Send buffer to remote server
 *
 * @param buffer: the buffer string to send
 * @param buffer_size: size of buffer in bytes
 * @param addr_opt: addr string containing both the ip and port
 *
 * @return int err // 0 means OK
 */
int gt_send(char *buffer, int buffer_size, char *addr_opt)
{
    int socket_desc, port;
    struct sockaddr_in server;
    char *addr, *port_str, server_reply[2], whole_addr[32];

    char en_addr[32];
    strcpy(en_addr, addr_opt);

    addr = strtok(addr_opt, ":");
    port_str = strtok(NULL, ":");

    port = strtol(port_str, &port_str, 10);

    // Create socket with AF_INET(IPv4) and SOCK_STREAM(TCP)
    socket_desc = socket(AF_INET, SOCK_STREAM, 0); // 0 means IP protocol

    int max_buffsize = MAX_BUFFER_SZ;
    setsockopt(socket_desc, SOL_SOCKET, SO_RCVBUF, &max_buffsize, sizeof(max_buffsize));

    if (socket_desc == -1)
    {
        printf("Cloud not create socket !\n");
    }

    printf("Transmitting to %s\n", en_addr);

    // Set server address
    server.sin_addr.s_addr = inet_addr(addr);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    // Connect to remote server
    if (connect(socket_desc, ( struct sockaddr *)&server, sizeof(server)) < 0)
    {
        puts("connect error");
        return 1;
    }

    // Send data to server
    if (send(socket_desc, buffer, buffer_size, 0) < 0)
    {
        puts("Send failed\n");
        return 1;
    }

    // Receive the result from the server
    if (recv(socket_desc, server_reply, 1, 0) < 0)
    {
        printf("Receive failed\n");
    }

    close(socket_desc);

    // Interpret result as an integer, 0 means OK
    int trans_err = strtol(port_str, NULL, 10);

    return trans_err;
}

/**
 * Encrypt file using libgcrypt and send/write to file
 *
 * @param filename : input filename
 * @param output_filename : output filename
 * @param key : key with witch we will encrypt and generage hmac
 * @param local_mode: if it is in local mode
 * @param addr_opt: addr option with ip and port number
 *
 * @return int err
 */
int gt_encrypt(char *filename, char *output_filename, uint8_t *key, int local_mode, char *addr_opt)
{
    // Init encode cipher
    gcry_cipher_hd_t cipher;
    gcry_error_t err;
    uint8_t iv[16];
    size_t blkLength = gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES128); // 16
    size_t keyLength = gcry_cipher_get_algo_keylen(GCRY_CIPHER_AES128); // 16

    // init iv with default value
    int i;
    for (i = 0; i < 16; i++)
    {
        iv[i] = (uint8_t)DEFAULT_IV;
    }

    err = gcry_cipher_open(&cipher, GCRY_CIPHER_AES128, GCRY_CIPHER_MODE_CBC, 0);
    if (err)
    {
        printf("open cipher error\n");
        handle_error(err);
    }
    err = gcry_cipher_setkey(cipher, key, 16);
    if (err)
    {
        printf("set key error\n");
        handle_error(err);
    }
    err = gcry_cipher_setiv(cipher, iv, blkLength);
    if (err)
    {
        printf("set iv error\n");
        handle_error(err);
    }

    // Compute file size
    int file_size = fsize(filename);
    int size_mod = 16 - (file_size % blkLength);
    int buffer_size = file_size + size_mod;

    char in_text[buffer_size];
    char *in_buffer = malloc(buffer_size);

    FILE *fp;
    int bytes;

    fp = fopen(filename, "r");

    // read file content
    bytes = fread(in_text, 1, buffer_size, fp);

    // Append null characters to the end of buffer
    while(bytes < buffer_size)
    {
        in_text[bytes++] = 0;
    }

    // Encrypt file content
    err = gcry_cipher_encrypt(cipher, in_buffer, buffer_size, in_text, buffer_size);

    fclose(fp);
    gcry_cipher_close(cipher);

    if (err)
    {
        printf("encrypt error\n");
        handle_error(err);
        return 1;
    }

    printf("Sucessfully encrypted %s to %s (%d bytes written).\n", filename, output_filename, buffer_size);

    // Compute HMAC from encrypted data
    size_t hmac_size = gcry_md_get_algo_dlen(GCRY_MD_SHA512);
    gcry_md_hd_t hd;

    char *buffer = malloc(buffer_size*sizeof(char));

    // new_stream_buffer is to hold the entire buffer: encrypted data + hmac
    char *new_stream_buffer = malloc((buffer_size + hmac_size + 1) * sizeof(char));
    buffer = in_buffer;

    // Init hmac handler
    err = gcry_md_open(&hd, GCRY_MD_SHA512, GCRY_MD_FLAG_HMAC);
    if (err) {
        handle_error(err);
        printf("gcry mac open error");
    }

    err = gcry_md_setkey(hd, key, sizeof(key));
    if (err) {
        handle_error(err);
        printf("gcry mac set key error");
    }

    gcry_md_write(hd, buffer, buffer_size);

    char hmac_buffer[hmac_size];
    memcpy(hmac_buffer, gcry_md_read(hd, 0), hmac_size);

    gcry_md_close(hd);

    // Combine encrypted data ($buffer) and hmac ($hamc_buffer)
    memcpy(new_stream_buffer, buffer, buffer_size);
    memcpy(new_stream_buffer+buffer_size, hmac_buffer, hmac_size);

    int stream_size = buffer_size + hmac_size;

    // Write to file or send through socket
    if ( ! local_mode ) {
        err = gt_send(new_stream_buffer, stream_size, addr_opt);
        if (! err) {
            printf("Successfully received\n");
        } else {
            printf("Transmittion failed with error code %d", err);
        }
    } else if (local_mode) {
        err = gt_write(new_stream_buffer, stream_size, output_filename);
        if (! err) {
            printf("Successfully write to file %s.\n", output_filename);
        } else {
            printf("Failed write to file %s.\n", output_filename);
        }
    }

    return 0;
}


int main(int argc, char *argv[])
{

    char *options = "d:lh?";
    char *input_filename;
    char passwd[MAX_PASSWD_SZ];
    uint8_t key[PASSWD_BUFFER_SZ];
    int i, c, err, socket_desc;
    char *addr_opt, *addr;
    int port;
    char *port_str;
    int daemon_mode = 0, local_mode = 0;

    // Check filename argument
    input_filename = argv[1];
    if ( *input_filename == '-')
    {
        printf("Filename required at first argument!\n");
        display_usage();
        return 1;
    }

    // if output file exist, return err code 33
    char output_filename[strlen(input_filename) + 4];
    snprintf(output_filename, strlen(input_filename) + 4, "%s%s", input_filename, ".gt");

    if (file_exist(output_filename)) {
        printf("Error: output file exist 33\n");
        return 33;
    }

    // set argment position from the third one
    optind = 2;
    while((c = getopt(argc, argv, options)) != -1)
    {
        switch(c) {
            case 'd':
                addr_opt = optarg;
                daemon_mode = 1;

                break;
            case 'l':
                local_mode = 1;
                break;
            case '?':
            case 'h':
                display_usage();
                break;
        }
    }

    // Ask user for password
    printf("Password: ");
    scanf("%[^\n]s", passwd);

    //derive key from passwd
    int derive_err = gcry_kdf_derive(passwd, strlen(passwd), GCRY_KDF_PBKDF2,
            GCRY_MD_SHA512, PBKDF2_SALT, sizeof(PBKDF2_SALT), 4096, sizeof(key), key);
    printf("Key: ");
    for (i = 0; i < PASSWD_BUFFER_SZ; i++) {
        printf("%X ", key[i]);
    }

    printf("\n");

    // encrypt file to output buffer struct
    err = gt_encrypt(input_filename, output_filename, key, local_mode, addr_opt);

    return err;
}
