#include <stdio.h>
#include <string.h>
#include <gcrypt.h>
#include <sys/stat.h> //stat
#include <errno.h> // fsize strerror

#define MAX_PASSWD_SZ 128
#define PASSWD_BUFFER_SZ 16
#define PBKDF2_SALT "NaCl"
#define DEFAULT_IV 5844

void handle_error(gcry_error_t err)
{
    fprintf (stderr, "Failure: %s/%s\n", gcry_strsource (err), gcry_strerror (err));
}

int file_exist(char *filename)
{
  struct stat buffer;
  return (stat (filename, &buffer) == 0);
}

off_t fsize(const char *filename) {
    struct stat st;

    if (stat(filename, &st) == 0)
        return st.st_size;

    fprintf(stderr, "Cannot determine size of %s: %s\n",
                        filename, strerror(errno));

    return -1;
}

int gt_write(char *buffer, int buffer_size, char *filename)
{
    FILE *fpout;
    fpout = fopen(filename, "w");
    int bytes = fwrite(buffer, 1, buffer_size, fpout);
    if (bytes < 0) {
        return 1;
    }
    fclose(fpout);
    return 0;
}
