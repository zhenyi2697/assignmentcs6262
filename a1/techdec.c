#include <stdio.h>
#include <string.h> //strlen
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr: convert string to int ip
#include <unistd.h> // write
#include <sys/stat.h> //stat
#include <sys/types.h>
#include <gcrypt.h>
#include <sys/ioctl.h>
#include <limits.h>

#include "gtscp.h"

/**
 * Display command usage
 */
void display_usage()
{
    printf("OVERVIEW: file encryption/transmission suite across network\n\n");
    printf("USAGE: \t techdec < filename > [-d < port >] [-l ]\nOPTIONS:\n");
    printf("-d <port>          listening port\n");
    printf("-l                 Local mode, only encrypt file\n");
    printf("-h, -?             Display help message\n");
}

/**
 * Decrypt buffer and save to file
 *
 * @param buffer: buffer to decrypt
 * @param buffer_size : size of buffer in bytes
 * @param key : key used to decrypt
 * @param filename: input file name
 *
 * @return int err // 0 means OK
 */
int gt_decrypt(char *buffer, int buffer_size, uint8_t *key, char *output_filename)
{

    // Compute encrytped text size and hamc size
    size_t hmac_size = gcry_md_get_algo_dlen(GCRY_MD_SHA512);
    size_t subbuf_size = buffer_size - hmac_size;
    char hmac_buffer_from_socket[hmac_size];
    char *subbuff = malloc(subbuf_size * sizeof(char));
    int i;

    // Split buffer into two parts
    // Store encrypted data in subbuff and hmac in hmac_buffer_from_socket
    memcpy(subbuff, buffer, subbuf_size);
    memcpy(hmac_buffer_from_socket, buffer+subbuf_size, hmac_size);

    // compute hmac from encrypted data
    gcry_md_hd_t hd;
    gcry_error_t err;

    err = gcry_md_open(&hd, GCRY_MD_SHA512, GCRY_MD_FLAG_HMAC);
    if (err) {
        handle_error(err);
        printf("Gcry mac open error");
    }

    err = gcry_md_setkey(hd, key, sizeof(key));
    if (err) {
        handle_error(err);
        printf("Gcry mac set key error");
    }

    gcry_md_write(hd, subbuff, subbuf_size);

    /* gcry_md_read(hd, hmac_buffer, &hmac_size); */
    char hmac_buffer[hmac_size];
    memcpy(hmac_buffer, gcry_md_read(hd, 0), hmac_size);

    gcry_md_close(hd);

    // Test if hmac is matched, if not, return err code 62
    int match = memcmp(hmac_buffer_from_socket, hmac_buffer, hmac_size);
    if (match != 0 ) {
        return 62;
    }

    // Decrypt buffer
    gcry_cipher_hd_t cipher;
    uint8_t iv[16];
    size_t blkLength = gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES128); // 16
    size_t keyLength = gcry_cipher_get_algo_keylen(GCRY_CIPHER_AES128); // 16

    // init iv
    for (i = 0; i < 16; i++)
    {
        iv[i] = (uint8_t)DEFAULT_IV;
    }

    err = gcry_cipher_open(&cipher, GCRY_CIPHER_AES128, GCRY_CIPHER_MODE_CBC, 0);
    if (err)
    {
        printf("Open cipher error\n");
        handle_error(err);
    }

    err = gcry_cipher_setkey(cipher, key, 16);
    if (err)
    {
        printf("Set key error\n");
        handle_error(err);
    }

    err = gcry_cipher_setiv(cipher, iv, blkLength);
    if (err)
    {
        printf("Set iv error\n");
        handle_error(err);
    }

    // Decrypt buffer in place
    err = gcry_cipher_decrypt(cipher, subbuff, subbuf_size, NULL, 0);
    if (err)
    {
        printf("Encrypt error\n");
        handle_error(err);
    }

    // write decrypted buffer to output file
    FILE *fpout;
    fpout = fopen(output_filename, "w");
    int bytes = fwrite(subbuff, 1, subbuf_size, fpout);
    fclose(fpout);

    printf("Successfully decrypted %s (%d bytes written).\n", output_filename, (int)subbuf_size);

    return 0;
}

int main(int argc, char *argv[])
{

    char *options = "d:lh?";
    char *input_filename;
    char *port_str;
    int port;
    int daemon_mode = 0, local_mode = 0;
    char passwd[MAX_PASSWD_SZ];
    uint8_t key[PASSWD_BUFFER_SZ];
    int i, err;

    int socket_desc, new_socket, c;
    struct sockaddr_in server, client;
    char response[2];

    // Check filename argument
    input_filename = argv[1];
    if ( *input_filename == '-')
    {
        printf("Filename required at first argument!\n");
        display_usage();
        return 1;
    }

    // Compute output file name
    char output_filename[strlen(input_filename) - 2];
    strncpy(output_filename, input_filename, strlen(input_filename) - 3);
    output_filename[strlen(input_filename) - 3] = '\0';

    if (file_exist(output_filename)) {
        printf("Error: output file exist 33\n");
        return 33;
    }

    // Set argment position from the third one
    optind = 2;

    // Read arguments
    while((c = getopt(argc, argv, options)) != -1)
    {
        switch(c) {
            case 'd':
                port_str = optarg;
                daemon_mode = 1;

                break;
            case 'l':
                local_mode = 1;
                break;
            case '?':
            case 'h':
                display_usage();
                break;
        }
    }

    if (local_mode) { // If is in local mode, just decrypt and write to file

        // Ask user for password
        printf("Password: ");
        scanf("%[^\n]s", passwd);

        //derive key from passwd
        int derive_err = gcry_kdf_derive(passwd, strlen(passwd), GCRY_KDF_PBKDF2,
                GCRY_MD_SHA512, PBKDF2_SALT, sizeof(PBKDF2_SALT), 4096, sizeof(key), key);
        printf("Key: ");
        for (i = 0; i < PASSWD_BUFFER_SZ; i++) {
            printf("%X ", key[i]);
        }

        printf("\n");

        // Open input file and read content into buffer
        FILE *fp;
        struct stat st;

        fp = fopen(input_filename, "r");
        if (fp == NULL) {
            printf("Unable to open file %s\n", input_filename);
            return 1;
        }

        fstat(fileno(fp), &st);
        char *buffer = malloc(st.st_size); // buffer contains the file content
        fread(buffer, sizeof(char), st.st_size, fp);
        fclose(fp);

        // decrypt buffer and write to file
        err = gt_decrypt(buffer, st.st_size, key, output_filename);

        if (err == 62) {
            printf("HMAC not match 62\n");
            return err;
        }

        return 0;

    } else if(daemon_mode) { // if in deamon_mode, listen to port and decrypt incoming buffer

        port = strtol(port_str, &port_str, 10);

        // Create socket with AF_INET(IPv4) and SOCK_STREAM(TCP)
        // 0 means IP protocol
        socket_desc = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_desc == -1)
        {
            printf("Cloud not create socket !\n");
        }

        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_family = AF_INET;
        server.sin_port = htons(port);

        // Bind to port
        if (bind(socket_desc, (struct sockaddr*)&server, sizeof(server)) < 0)
        {
            printf("Bind to port %d failed\nExit;\n", port);
            return 2;
        }

        // Listen
        listen(socket_desc, 3);

        // Accept incoming connections
        printf("Waiting for connections.\n");
        c = sizeof(struct sockaddr_in);

        // Accept incoming socket
        new_socket = accept(socket_desc, (struct sockaddr*)&client, (socklen_t*)&c);

        // IMPORTANT !!
        // Set maximum buffer size as 1024*1024 bytes = 1 MB
        // buffer greater than this will not be fully received

        int max_buffsize = MAX_BUFFER_SZ;
        setsockopt(new_socket, SOL_SOCKET, SO_RCVBUF, &max_buffsize, sizeof(max_buffsize));
        printf("Inbound file.\n");

        // Ask user for password
        printf("Password: ");
        scanf("%[^\n]s", passwd);

        //derive key from passwd
        int derive_err = gcry_kdf_derive(passwd, strlen(passwd), GCRY_KDF_PBKDF2,
                GCRY_MD_SHA512, PBKDF2_SALT, sizeof(PBKDF2_SALT), 4096, sizeof(key), key);

        printf("Key: ");
        for (i = 0; i < PASSWD_BUFFER_SZ; i++) {
            printf("%X ", key[i]);
        }

        printf("\n");

        // Compute buffer length
        int len = 0;
        ioctl(new_socket, FIONREAD, &len);
        char client_message[len];
        if (len > 0) {
          len = read(new_socket, client_message, len);
        }

        // decrypt buffer and write to file
        err = gt_decrypt(client_message, len, key, output_filename);

        // Reply to client
        snprintf(response, sizeof response, "%d", err);
        write(new_socket, response, strlen(response));

        close(socket_desc);
        /* close(new_socket); */

        if (err == 62) {
            printf("HMAC not match: 62\n");
            printf("Maybe buffer size limit reached (%d bytes)\n", MAX_BUFFER_SZ);
            close(socket_desc);
            /* close(new_socket); */
            return err;
        }

        if (new_socket < 0)
        {
            printf("Accept failed\n");
            return 1;
        }

    }

    return 0;
}
