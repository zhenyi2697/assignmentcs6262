#!/bin/sh

## This script tests the firewall from all devices
## You should copy it to all devices and run from each.

PUBIP=192.168.1.1
INET=10.0.2.16
WKSTN=172.16.1.2

if [ `hostname` = "6262-rtr" ]; then
	### Policy 6 (Deny the others)
	ping 172.168.1.2
	ping 10.0.2.16
	ping 192.168.1.1
fi

if [ `hostname` = "6262-wkstn" ]; then
	#run commands from workstation
	### Policy 3
	wget http://10.0.2.16

	### Policy 4
	ssh tux@192.168.1.1
	wget http://192.168.1.1
	nc 192.168.1.1 25
	nc -u 192.168.1.1 53

	### Policy 5
	ssh tux@172.16.1.100

	### Policy 6 (Deny the others)
	ping 172.16.1.100
	ping 10.0.2.16
	ping 192.168.1.1
fi

if [ `hostname` = "6262-pub" ]; then
	#run commands from public server
	### Policy 2
	nc -u 10.0.2.16 53
	nc 10.0.2.16 25

	### Policy 6 (Deny the others)
	ping 172.16.1.100
	ping 10.0.2.16
	ping 172.16.1.2

fi

if [ `hostname` = "6262-inet" ]; then
	#run commands from the "internet"
	### Policy 1
	wget http://192.168.1.1
	nc -u 192.168.1.1 53
	nc 192.168.1.1 25

	### Policy 6 (Deny the others)
	ping 172.16.1.100
	ping 192.168.1.1
	ping 172.16.1.2
fi


