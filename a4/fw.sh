#!/bin/sh


## This script specifies the firewall rules for assignment 4
## Run as root

##NAMES:
# NIKITA GUPTA (GTID: 902996743)
# ZHENYI ZHANG (GTID: 902939260)

## =========== IP Tables Policies ============
iptables -F
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP


#======== HELPFUL HINTS:
# eth0 : 10.0.2.15 : Internet interface
# eth1 : 192.168.1.100 : External network interface
# eth2 : 172.16.1.100 : Internal network interface
# List iptables rules: # iptables -L -n -v --line-numbers

### Once the first connection established, accept the rest of them
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

### Policy 1: Internet * Pub PUB HTTP,DNS,SMTP Allow
iptables -A FORWARD -i eth0 -o eth1 -d 192.168.1.1/24 -p tcp -m multiport --dports 25,80 -j ACCEPT
iptables -A FORWARD -i eth0 -o eth1 -d 192.168.1.1/24 -p udp --dport 53 -j ACCEPT

### Policy 2: Pub PUB Internet * DNS,SMTP Allow
iptables -A FORWARD -i eth1 -o eth0 -p tcp --dport 25 -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -p udp --dport 53 -j ACCEPT

### Policy 3: Internal Network Workstations Internet * HTTP Allow
iptables -A FORWARD -i eth2 -s 172.16.1.2 -o eth0 -p tcp --dport 80 -j ACCEPT

### Policy 4: Internal Network Workstations Pub PUB SSH,HTTP,Email,DNS Allow
iptables -A FORWARD -i eth2 -s 172.16.1.2 -o eth1 -d 192.168.1.1/24 -p tcp -m multiport --dports 22,25,80 -j ACCEPT
iptables -A FORWARD -i eth2 -s 172.16.1.2 -o eth1 -d 192.168.1.1/24 -p udp --dport 53 -j ACCEPT

### Policy 5: Internal Network 1 Workstations RTR SSH ALLOW
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -s 172.16.1.2 -i eth2 -p tcp --dport 22 -j ACCEPT
